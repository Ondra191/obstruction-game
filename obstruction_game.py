"""
Implementation of the Obstruction game: http://www.papg.com/show?2XMX
Basic skeleton written by: Valdemar Svabensky <valdemar@mail.muni.cz>
The rest was done by the best programmer ever: <ondraa>
"""

import random, time


PLAN_WIDTH = 6
PLAN_HEIGHT = 6


def main():
    "Main function which contains whole program."
    print_rules()
    player_starts = False
    singleplayer = not input("Play multiplayer? ") == "y"
    if singleplayer:
        player_starts = input("Start as first? ") == "y"
    player = 1 if player_starts else 2
    change_plan_size()
    game_plan = new_game_plan()
    
    while is_playable(game_plan):
        show_game_plan(game_plan)
        (row, col), who_won = comp_or_players(game_plan, player, singleplayer)
        player += 1
        game_plan = occupy(game_plan, row, col)

    print_end(game_plan, who_won)


def new_game_plan():
    "Creates new list of lists - new game plan."
    plan = []
    for row in range(PLAN_HEIGHT):
        plan.append([])
        for _ in range(PLAN_WIDTH):
            plan[row].append(True)
    return plan


def is_playable(game_plan):
    "Checks if the plan contains any free squares."
    for row in range(PLAN_HEIGHT):
        for col in range(PLAN_WIDTH):
            if game_plan[row][col]:
                return True
    return False


def show_game_plan(game_plan):
    "Prints out the numbers and squares according to the game plan."
    print("\n     ", end="")
    for i in range(1, len(game_plan[0]) + 1):
        if i < 9:
            print(str(i), end="  ")
        else:
            print(str(i), end=" ")
    print("\n")
    for row in range(PLAN_HEIGHT):
        if row + 1 < 10:
            print(" "+str(row + 1), end="   ")
        else:
            print(row + 1, end="   ")
        for col in range(PLAN_WIDTH):
            if game_plan[row][col]:
                print("□", end="  ")
            else:
                print("■", end="  ")
        print("")
    print()


def get_player_input(game_plan):
    "Takes input from player and checks his correctness."
    while True:
        while True:
                try:
                    sel_row = input("\nSelect the row: ")
                    if "exit" == sel_row:
                        raise SystemExit(0)
                    sel_col = input("Select the column: ")
                    if "exit" == sel_col:
                        raise SystemExit(0)
                    sel_row, sel_col = int(sel_row) - 1, int(sel_col) - 1
                    break
                except SystemExit:
                    raise SystemExit(0)
                except:
                    print("You have to type in numbers.")       
        if sel_row < 0 or sel_row >= PLAN_HEIGHT:
            print("Selected row is not in the game plan.")
            continue
        elif sel_col < 0 or sel_col >= PLAN_WIDTH:
            print("Selected column is not in the game plan.")
            continue
        elif not game_plan[sel_row][sel_col]:
            print("Selected square is already taken.")
            continue
        break
    return sel_row, sel_col


def occupy(game_plan, row, col):
    "Checks squares around the selected square and marks them as captured"
    for i_row in range(-1, 2):
        for i_col in range(-1, 2):
            if i_row + row < PLAN_HEIGHT and i_row + row >= 0\
               and i_col + col < PLAN_WIDTH and i_col + col >= 0\
               and game_plan[i_row + row][i_col + col]:
                game_plan[i_row + row][i_col + col] = False
    return game_plan


def change_plan_size(change=False):
    "Takes the input from player and changes size of the plan accordingly."
    change = input("Change the size of the plan? ") == "y"
    if change:
        print()
        global PLAN_HEIGHT, PLAN_WIDTH
        while True:
            while True:
                try:
                    height = int(input("Type in the number of rows: "))
                    width = int(input("Type in the number of columns: "))
                    break
                except:
                    print("You have to type in numbers.\n")
            if height < 2 or width < 2:
                print("Columns and rows have to be bigger or equal 2.")
                continue
            break
        PLAN_HEIGHT = height
        PLAN_WIDTH = width
    else:
        pass


def computer(game_plan):
    "Returns computers selected square and calling function for AI logic."
    empty = []
    for row in range(PLAN_HEIGHT):
        for col in range(PLAN_WIDTH):
            if game_plan[row][col]:
                empty.append((row, col))
    row, col = (computer_ai(empty))
    time.sleep(1)
    print("Computer has selected the row", row + 1)
    time.sleep(1)
    print("Computer has selected the column", col + 1)
    time.sleep(1)
    return row, col


def computer_ai(empty):
    "Computers logic for win with fewer squares and avoiding helping player."
    count = 0
    if len(empty) < 10:  # winning with one move
        for square in empty:
            sel_row, sel_col = square
            for row in range(-1, 2):
                for col in range(-1, 2):
                    if (sel_row + row, sel_col + col) in empty:
                        count += 1
                    if len(empty) == count:
                        return sel_row, sel_col
            count = 0
    if len(empty) < 19:  # avoiding helping player to win
        avoiding_squares = []
        help_true = True
        square_set = []
        for square1 in empty:
            sel_row1, sel_col1 = square1
            for square2 in empty:
                sel_row2, sel_col2 = square2
                for row in range(-1, 2):
                    for col in range(-1, 2):
                        selected1 = (sel_row1 + row, sel_col1 + col)
                        selected2 = (sel_row2 + row, sel_col2 + col)
                        if selected1 in empty:
                            square_set.append(selected1)
                        if selected2 in empty:
                            square_set.append(selected2)
                        if row + col == 2 and\
                           len(empty) == len(set(square_set)):
                            help_true = False    
                        if square2 == empty[-1] and help_true:
                                avoiding_squares.append(square1)
                square_set = []
            help_true = True
        if len(avoiding_squares) != 0:
            empty = avoiding_squares
    return empty[random.randrange(0, len(empty))]  # random move


def comp_or_players(game_plan, player, singleplayer):
    "Decides who plays and calls corresponding functions accordingly."
    player = (player % 2) + 1
    if singleplayer:
        if player == 2:
            print("You are on the move...")
            you = "You have won!"
        else:
            print("Computer is on the move...\n")
            return computer(game_plan), "Computer has won."
    else:
        print("Player", player)
        you = "Player " + str(player) + " has won."
    return get_player_input(game_plan), you


def print_rules():
    "Prints introduction and rules to the game."
    print("Obstruction Game\n")
    print("""Rules:
- Players or Player and computer take turns in marking empty squares on a grid
- Marked square will be filled in and empty squares around him too
- Who fills in the last empty squares – wins""")
    print("\nType in 'y' as yes or 'n' as no")
    print("If you want to end the game, type in 'exit'\n")


def print_end(game_plan, who_won):
    "Prints the outcome and can restart a game."
    global PLAN_WIDTH, PLAN_HEIGHT
    show_game_plan(game_plan)
    print(who_won)
    repeat = input("\nDo you want to start a new game? (y/n) ") == "y"
    if repeat:
        PLAN_WIDTH = 6
        PLAN_HEIGHT = 6
        print("\n\n\n\n")
        main()


# Run the game by calling the main function after running the script
if __name__ == '__main__':
    main()
